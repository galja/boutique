package com.epam.control;

import com.epam.cart.Cart;
import com.epam.utils.BouquetType;
import com.epam.utils.Utils;
import com.epam.view.Displayable;
import com.epam.model.user.Performable;
import com.epam.view.StdView;
import com.epam.model.user.Updatable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CallCenter implements Publishable{
  private List<Updatable> observers;
  private Map<Integer, Performable> exeMenu;
  private Displayable monitor;
  private Updatable currUser;
  private BufferedReader br;
  private Cart currCart;

  public CallCenter() {
    br = new BufferedReader(new InputStreamReader(System.in));
    exeMenu = new LinkedHashMap<>();
    exeMenu = prepareExeMenu();
    monitor = new StdView();
    observers = new LinkedList<>();
  }

  public void setCurrUser(Updatable currUser) {
    this.currUser = currUser;
  }

  public void phoneCall(Updatable user) {
    setCurrUser(user);
    subscribeObs(user);
    currCart = new Cart(user);
    //startBroadcast();
    getChoice();
  }

  private void getChoice() {
    showStartMenu();
    String input = "";

    int upMenuBound = exeMenu.size();
    try {
      input = br.readLine();
      if(input.isEmpty() || !input.trim().matches("[0-" + upMenuBound +"]")) {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      Utils.logger.error(Utils.CONS_R_ERR);
    } catch (IllegalArgumentException e) {
      Utils.logger.warn(Utils.NO_MENU_ITEM);
      getChoice();
    }
    processChoice(Integer.parseInt(input));
  }

  private void processChoice(int c) {
    exeMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey() == c)
        .forEach(i -> i.getValue().perform());
  }

  private Map<Integer, Performable> prepareExeMenu(){
    Map<Integer, Performable> menu = new LinkedHashMap<>();
    menu.put(0, this::exitApp);
    menu.put(1, this::makeCatalogChoice);
    menu.put(2, this::makeOccasionalChoice);
    menu.put(3, this::makeExtraChoice);
    return menu;
  }

  public void subscribeObs(Updatable o) {
    observers.add(o);
  }

  public void removeObs(Updatable o) {
    if(observers.indexOf(o) >= 0) {
      observers.remove(o);
    }
  }

  public void notifyObs(int d) {
    observers
        .forEach(o -> o.update(d));
  }

  private void exitApp() {
    System.exit(0);
  }

  private void showStartMenu() {
    Utils.logger.info(Utils.WELC_MSG + currUser);
    Utils.getUserMenu()
        .get()
        .map(i -> i + "\n")
        .forEach(Utils.logger::info);
    Utils.logger.info(Utils.MAKE_CHOICE);
  }

  private void startBroadcast() {
    Runnable broadcast = () -> {
      try {
        Thread.sleep(Utils.START_BROADCAST_TIME);
      } catch (InterruptedException e) {
        Utils.logger.error(Utils.INTP_ERR);
      }
      notifyObs(Utils.TEMP_DISCOUNT);
    };
    ExecutorService eServ = Executors.newSingleThreadExecutor();
    eServ.execute(broadcast);
    eServ.shutdown();
  }

  public void makeCatalogChoice() {
    monitor.display(showCatalog());
    monitor.display(Utils.MAKE_CHOICE);
    String input = "";
    int choice = 0;
    try {
      input = br.readLine();
      if(input.isEmpty() || !input.matches("[0-3]")) {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      Utils.logger.error(Utils.INTP_ERR);
    } catch (IllegalArgumentException e) {
      Utils.logger.warn(Utils.NO_MENU_ITEM);
      makeCatalogChoice();
    }
    choice = Integer.parseInt(input);
    if(choice == 0) {
      getChoice();
    } else {
      choice--;
      for(BouquetType b : BouquetType.values()) {
        if(b.ordinal() == choice) {
          currCart.setCatalogOrder(b);
        }
      }
      makeExtraChoice();
    }
  }

  private void showCelebr() {
    String output = "";

    monitor.display(output);
  }

  private void showExtraService() {
    String output = "";

    monitor.display(output);
  }

  private String showCatalog() {
    String catalog = "";
    monitor.display(Utils.TODAY_OFF);
    for(int i = 0; i < BouquetType.values().length - 1; i++) {
      catalog += i+1 + ". " + BouquetType.values()[i].getbName() + "\n";
    }
    catalog += Utils.SUB_EXIT;
    return catalog;
  }



  private void makeOccasionalChoice() {
    showExtraService();
  }

  private void makeExtraChoice() {}
}
