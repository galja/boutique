package com.epam.control;

import com.epam.model.user.Updatable;

public interface Publishable {
  void subscribeObs(Updatable o);
  void removeObs(Updatable o);
}
