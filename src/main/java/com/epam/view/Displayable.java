package com.epam.view;

public interface Displayable {
  void display(String data);
}
