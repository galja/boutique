package com.epam.view;

import com.epam.utils.Utils;

public class StdView implements Displayable{

  public void display(String input) {
    Utils.logger.info(input);
  }
}
