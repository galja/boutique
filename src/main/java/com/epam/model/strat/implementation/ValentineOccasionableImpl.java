package com.epam.model.strat.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.bouquets.StandardBouquet;
import com.epam.model.strat.Occasionable;

import static com.epam.utils.Utils.*;

public class ValentineOccasionableImpl implements Occasionable {
    private String personsName;

    public ValentineOccasionableImpl(String personsName) {
        this.personsName = personsName;
    }

    @Override
    public Bouquet prepare() {
        return getBouquet(VALENTINE_BOUQUET_PRICE, this.personsName, VALENTINE_BOUQUET);
    }
}
