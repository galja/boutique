package com.epam.model.strat.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.strat.Occasionable;

import static com.epam.utils.Utils.*;


public class WeddingOccasionableImpl implements Occasionable {
    private String personsName;

    public WeddingOccasionableImpl(String personsName) {
        this.personsName = personsName;
    }

    @Override
    public Bouquet prepare() {
        return getBouquet(WEDDING_BOUQUET_PRICE, this.personsName, WEDDING_BOUQUET);
    }
}
