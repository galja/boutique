package com.epam.model.strat.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.bouquets.StandardBouquet;
import com.epam.model.strat.Occasionable;

import static com.epam.utils.Utils.*;

public class BirthdayOccasionableImpl implements Occasionable {
    private String personsName;

    public BirthdayOccasionableImpl(String personsName) {
        this.personsName = personsName;
    }

    @Override
    public Bouquet prepare() {
        return getBouquet(BIRTHDAY_BOUQUET_PRICE, this.personsName, BIRTHDAY_BOUQUET);
    }
}
