package com.epam.model.strat;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.bouquets.StandardBouquet;
import static com.epam.utils.Utils.CUSTOM_BOUQUET;

public interface Occasionable {
    Bouquet prepare();

    default Bouquet getBouquet(int bouquetPrice, String personsName, String bouquetType) {
        Bouquet b = new StandardBouquet();
        b.setPrice(bouquetPrice);
        b.setName(CUSTOM_BOUQUET + personsName);
        b.setType(bouquetType);
        return b;
    }
}

