package com.epam.model.bouquets;

import static com.epam.utils.Utils.logger;

public class FloreBouquet extends Bouquet {

    public FloreBouquet(String name, String type, int price){
        super(name, type, price);
    }

    public FloreBouquet() {
    }

    @Override
    public void wrap() {
        logger.info("Bouquet is wrapped");
    }

    @Override
    public void addRibbon() {
        logger.info("Added ribbon to bouquet");
    }

    @Override
    public void addPostcard() {
        logger.info("Added paper");
    }
}
