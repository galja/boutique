package com.epam.model.bouquets;

import static com.epam.utils.Utils.logger;

public class RoseBouquet extends Bouquet {

    public RoseBouquet() {
    }

    public RoseBouquet(String name, String type, int price){
        super(name, type, price);
    }

    @Override
    public void wrap() {
        logger.info("Bouquet is wrapped");
    }

    @Override
    public void addRibbon() {
        logger.info("Added ribbon to bouquet");
    }

    @Override
    public void addPostcard() {
        logger.info("Added paper");
    }
}
