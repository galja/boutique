package com.epam.model.bouquets;

public abstract class Bouquet {
    private double price;
    private String name;
    private String type;
    private String locationBouquet;

    public Bouquet(){}

    public Bouquet(String name, String type, int price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLocationBouquet() {
        return locationBouquet;
    }

    public void setLocationBouquet(String locationBouquet) {
        this.locationBouquet = locationBouquet;
    }

    public abstract void wrap();

    public abstract void addRibbon();

    public abstract void addPostcard();

    @Override
    public String toString() {
        return "Bouquet{" +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                "price=" + price +
                '}';
    }
}
