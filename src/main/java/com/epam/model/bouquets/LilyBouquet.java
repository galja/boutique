package com.epam.model.bouquets;

import static com.epam.utils.Utils.logger;

public class LilyBouquet extends Bouquet {

    public LilyBouquet(String name, String type, int price) {
        super(name, type, price);
    }

    public LilyBouquet() {
    }

    @Override
    public void wrap() {
        logger.info("Bouquet is wrapped");
    }

    @Override
    public void addRibbon() {
        logger.info("Added ribbon to bouquet");
    }

    @Override
    public void addPostcard() {
        logger.info("Added paper");
    }
}
