package com.epam.model.bouquets;

import static com.epam.utils.Utils.logger;

public class StandardBouquet extends Bouquet {

    public StandardBouquet(){
        super("standard", "standard", 100);
    }

    @Override
    public void wrap() {
        logger.info("Bouquet is wrapped");
    }

    @Override
    public void addRibbon() {
        logger.info("Added ribbon to bouquet");
    }

    @Override
    public void addPostcard() {
        logger.info("Added paper");
    }
}
