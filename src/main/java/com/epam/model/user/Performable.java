package com.epam.model.user;

@FunctionalInterface
public interface Performable {
  void perform();
}
