package com.epam.model.user;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.model.strat.Occasionable;
import com.epam.utils.BouquetType;
import com.epam.utils.Card;
import com.epam.utils.Location;
import com.epam.utils.Utils;
import java.util.LinkedList;
import java.util.List;

public class User implements Updatable {
  private String name;
  private Card myCard;
  private Location country;
  private int tempDisc;


  public User(String n, Card card, Location c) {
    name = n;
    myCard = card;
    country = c;
  }

  public String getName() {
    return name;
  }

  public Card getMyCard() {
    return myCard;
  }

  public Location getCountry() {
    return country;
  }

  public int getTempDisc() {
    return tempDisc;
  }

  public void update(int d) {
    tempDisc = d;
    Utils.logger.info(String.format(Utils.DISC_RECEIVE_FORMAT, getName(), getTempDisc()));
  }

  @Override
  public String toString() {
    String output = "";
    output += getName();
    switch (getCountry()) {
      case DE:
        output += Utils.FRB;
        break;
      case FR:
        output += Utils.FRP;
        break;
      case GB:
        output += Utils.FRL;
        break;
        default:
          break;
    }
    return output;
  }
}
