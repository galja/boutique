package com.epam.model.decorator.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.utils.Card;

import static com.epam.utils.Utils.HUNDRED_PERCENT;

public class Discount extends BoutiqueDecorator {

    public Discount(Bouquet bouquet, Card card) {
        super(bouquet, card);
        makeDiscount(this.getCard().getBaseDicount());
    }

    public Discount(Bouquet bouquet, int discount) {
        super(bouquet);
        makeDiscount(discount);
    }

    private void makeDiscount(int discount) {
        this.getBouquet()
                .setPrice(this.getBouquet()
                        .getPrice() * (1 - discount / HUNDRED_PERCENT));
    }

}
