package com.epam.model.decorator.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.utils.Card;

public class PriorityShipping  extends Delivery{

    public PriorityShipping(Bouquet bouquet, Card card) {
        super(bouquet, card);
    }

    @Override
    public void deliver() {
        this.setPrice(this.getPrice() +doubleCost(this.getDeliveryCost(this.getCard())));
    }

    private double doubleCost(double cost){
        return cost*2;
    }

    @Override
    public String toString() {
        return super.toString()+"\nPriority shipping";
    }
}
