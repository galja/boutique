package com.epam.model.decorator.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.utils.Card;

public class StandardShipping extends Delivery {
    public StandardShipping(Bouquet bouquet, Card card) {
        super(bouquet, card);
    }

    public void deliver() {
        this.setPrice(this.getPrice() +this.getDeliveryCost(this.getCard()));
    }

    @Override
    public String toString() {
        return super.toString()+"\nStandard shipping";
    }
}
