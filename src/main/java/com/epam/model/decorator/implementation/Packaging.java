package com.epam.model.decorator.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.utils.Card;

import static com.epam.utils.Utils.BOX_PRICE;
import static com.epam.utils.Utils.HUNDRED_PERCENT;


public class Packaging extends BoutiqueDecorator {

    public Packaging(Bouquet bouquet, Card card) {
        super(bouquet, card);
    }

    private void addBoxPrice() {
        this.setPrice(this.getPrice() + BOX_PRICE * (1 - getCard().getBaseDicount() / HUNDRED_PERCENT));
    }

    @Override
    public String toString() {
        return super.toString()+"\n"+ "Packaging";
    }
}
