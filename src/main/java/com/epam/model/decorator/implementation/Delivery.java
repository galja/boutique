package com.epam.model.decorator.implementation;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.utils.Card;

import static com.epam.utils.Utils.*;

public abstract class Delivery extends BoutiqueDecorator {

    public Delivery(Bouquet bouquet, Card card) {
        super(bouquet, card);
    }

    public abstract void deliver();

    double getDeliveryCost(Card cardType) {
        double cost;
        switch (cardType) {
            case GOLD:
                cost = GOLD_DELIVERY_PRICE;
                break;
            case SILVER:
                cost = SILVER_DELIVERY_PRICE;
                break;
            case SOCIAL:
                cost = STANDARD_DELIVERY_PRICE;
                break;
            default:
                cost = STANDARD_DELIVERY_PRICE;
        }
        return cost;
    }
}
