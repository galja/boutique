package com.epam.model.decorator;

import com.epam.model.bouquets.Bouquet;
import com.epam.utils.Card;

public abstract class BoutiqueDecorator extends Bouquet {
    private Bouquet bouquet;
    private Card card;

    public BoutiqueDecorator(Bouquet bouquet, Card card) {
        this.bouquet = bouquet;
        this.card = card;
    }

    public BoutiqueDecorator(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    @Override
    public void wrap() {
        bouquet.wrap();
    }

    @Override
    public void addRibbon() {
        bouquet.addRibbon();
    }

    @Override
    public void addPostcard() {
        bouquet.addRibbon();
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "BoutiqueDecorator{" +
                "bouquet=" + bouquet +
                '}';
    }
}
