package com.epam.model.boutiques;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.strat.Occasionable;

public class LondonBoutique extends GreenBoutique {

    @Override
    public Bouquet order(Occasionable o) {
        Bouquet b = create(o);
        b.wrap();
        b.addPostcard();
        b.addRibbon();
        return b;
    }

    private Bouquet create(Occasionable o) {
        return o.prepare();
    }
}
