package com.epam.model.boutiques;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.model.strat.Occasionable;
import com.epam.utils.Card;

import java.util.List;

public abstract class GreenBoutique {

    public abstract Bouquet order(Occasionable o, List<BoutiqueDecorator> decorators, Card card);
//    public abstract Bouquet order(Bouquet b, );
}
