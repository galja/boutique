package com.epam.model.boutiques;

import com.epam.model.bouquets.*;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.model.decorator.implementation.Discount;
import com.epam.model.decorator.implementation.Packaging;
import com.epam.model.decorator.implementation.PriorityShipping;
import com.epam.model.decorator.implementation.StandardShipping;
import com.epam.model.strat.Occasionable;
import com.epam.utils.*;

import java.util.List;

import static com.epam.utils.Utils.MADE_IN;

public class ParisBoutique extends GreenBoutique {
    private Bouquet bouquet;

    @Override
    public Bouquet order(Occasionable o, List<BoutiqueDecorator> decorators) {
        Bouquet b = create(o);
        b.wrap();
        b.addPostcard();
        b.addRibbon();
        return b;
    }

    public Bouquet order(BouquetType bouquetType, List<DecoratorType> decorators, Card card) {
        for (DecoratorType decoratorType : decorators) {
            switch (bouquetType) {
                case FB:
                    bouquet = setCity(decorate(new FloreBouquet(), decoratorType, card));
                case LB:
                    bouquet = setCity(decorate(new LilyBouquet(), decoratorType, card));
                case RB:
                    bouquet = setCity(decorate(new RoseBouquet(), decoratorType, card));
                case SB:
                    bouquet = setCity(decorate(new StandardBouquet(), decoratorType, card));
                default:
                    bouquet = setCity(decorate(new StandardBouquet(), decoratorType, card));
            }
        }
        return bouquet;
    }

    private Bouquet create(Bouquet bouquet, DecoratorType decoratorType, Card card) {
        switch (decoratorType) {
            case DISCOUNT:
                return new Discount(bouquet, card) {
                };
            case PACKAGING:
                return new Packaging(bouquet, card);
            case PRIORITY_SHIPPING:
                return new PriorityShipping(bouquet, card);
            case STANDARD_SHIPPING:
                return new StandardShipping(bouquet, card);
            default:
                return bouquet;
        }
    }


    private Bouquet create(StratType stratType, DecoratorType decoratorType, Card card) {
        switch (stratType) {
            case BD:
                return new Discount(bouquet, card) {
                };
            case PACKAGING:
                return new Packaging(bouquet, card);
            case PRIORITY_SHIPPING:
                return new PriorityShipping(bouquet, card);
            case STANDARD_SHIPPING:
                return new StandardShipping(bouquet, card);
            default:
                return bouquet;
        }

        return setCity(o.prepare());
    }

    private Bouquet create(BoutiqueDecorator boutiqueDecorat) {
        return null;
    }

    private Bouquet setCity(Bouquet bouquet) {
        bouquet.setLocationBouquet(MADE_IN + City.PARIS.getCity());
        return bouquet;
    }


}
