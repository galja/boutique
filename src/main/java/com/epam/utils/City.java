package com.epam.utils;

public enum City {
    LONDON("London"), PARIS("Paris");
    private String city;

    City(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }
}
