package com.epam.utils;

public enum DecoratorType {
    DISCOUNT("Count"), PACKAGING("Packaging"), PRIORITY_SHIPPING("gfd"), STANDARD_SHIPPING("dsf");
    private String decoratorType;

    DecoratorType(String decoratorType) {
        this.decoratorType = decoratorType;
    }

    public String getDecoratorType() {
        return decoratorType;
    }
}
