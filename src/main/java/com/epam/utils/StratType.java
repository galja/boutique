package com.epam.utils;

public enum StratType {

  VAL("Valentine`s day composition"), WED("Wedding flowered rings"),
  BD("Birthday 100 roses bunch");

  private String name;

  StratType(String n) {
    name = n;
  }

  public String getName() {
    return name;
  }
}
