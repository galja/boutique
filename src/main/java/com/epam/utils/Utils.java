package com.epam.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {
  public static Logger logger = LogManager.getLogger(Utils.class);
  private static List<String> userMenu;
  public static final String DISC_RECEIVE_FORMAT = "%s have new additional discount %d%n";
  public static final String FRL = " from London\n";
  public static final String FRP = " from Paris\n";
  public static final String FRB = " from Berlin\n";
  public static final String WELC_MSG = "Welcome to Green Boutique World Wide\n";
  public static final String INTP_ERR = "Thread execution error";
  public static int START_BROADCAST_TIME = 5000;
  public static final String MAKE_CHOICE = "What would you like to order: ";
  public static final String CONS_R_ERR = "Error while reading from console";
  public static final String BIRTHDAY_ACCESOURIES = "Birthday candles";
  public static final double SILVER_DELIVERY_PRICE = 200;
  public static final double GOLD_DELIVERY_PRICE = 0;
  public static final double STANDARD_DELIVERY_PRICE = 300;
  public static final double BOX_PRICE = 20.0;
  public static final String NO_MENU_ITEM = "\n--No such menu item--\n";
  public static final String BIRTHDAY_BOUQUET = "Birthday bouquet";
  public static final String VALENTINE_BOUQUET = "Birthday bouquet";
  public static final String WEDDING_BOUQUET = "Birthday bouquet";
  public static final String CUSTOM_BOUQUET = "Bouquet for ";
  public static final double SILVER_PRICE = 200;
  public static final double GOLD_PRICE = 0;
  public static final double HUNDRED_PERCENT = 100.0;
  public static final int TEMP_DISCOUNT = 10;
  public static final int BIRTHDAY_BOUQUET_PRICE = 1000;
  public static final int VALENTINE_BOUQUET_PRICE = 1200;
  public static final int WEDDING_BOUQUET_PRICE = 1200;
  public static final String TODAY_OFF = "\nToday`s offer is:\n";
  public static final String SUB_EXIT = "0. To exit\n";
  public static final String MADE_IN = "\nmade in ";

  static {
    userMenu = new LinkedList<>();
    userMenu.add("1. Order standard bouquet");
    userMenu.add("2. Order occasional bouquet");
    //userMenu.add("3. Order extra services");
    userMenu.add("0. Disconnect from CallCenter");
  }

  private Utils(){}

  public static Supplier<Stream<String>> getUserMenu() {
    return () -> userMenu.stream();
  }
}
