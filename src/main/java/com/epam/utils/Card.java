package com.epam.utils;

public enum Card {
  GOLD(40), SILVER(20), SOCIAL(10);
  private int baseDicount;

  Card(int baseDicount) {
    this.baseDicount = baseDicount;
  }

  public int getBaseDicount(){
    return this.baseDicount;
  }
}
