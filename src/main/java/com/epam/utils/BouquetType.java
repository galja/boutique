package com.epam.utils;

public enum BouquetType {
  RB("Rose Bouquet"), FB("Flore Bouquet"),
  LB("Lily Bouquet"), SB("Standard Bouquet");
  private String bName;

  BouquetType(String name) {
    bName = name;
  }

  public String getbName() {
    return bName;
  }
}
