package com.epam.cart;

import com.epam.model.bouquets.Bouquet;
import com.epam.model.decorator.BoutiqueDecorator;
import com.epam.model.strat.Occasionable;
import com.epam.model.user.Updatable;
import com.epam.model.user.User;
import com.epam.utils.BouquetType;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Cart {
  private User client;
  private Optional<BouquetType> catalogOrder;
  private Optional<Occasionable> celebrationOrder;
  private List<BoutiqueDecorator> extraOrder;
  private List<Bouquet> totalOrder;

    public Cart(Updatable c) {
      client = (User)c;
      catalogOrder = Optional.empty();
      celebrationOrder = Optional.empty();
      extraOrder = new LinkedList<>();
      totalOrder = new LinkedList<>();
    }

    public void setCatalogOrder(BouquetType catBt) {
      catalogOrder = Optional.ofNullable(catBt);
    }

    public void setCelebrationOrder(Occasionable celBt) {
      celebrationOrder = Optional.ofNullable(celBt);
    }

    public void setExtraOrder(BoutiqueDecorator extraServ) {
      extraOrder.add(extraServ);
    }

    public void setTotalOrder(Bouquet item) {
      totalOrder.add(item);
    }

    public Optional<BouquetType> getCatalogOrder() {
      return catalogOrder;
    }

    public Optional<Occasionable> getCelebrationOrder() {
      return celebrationOrder;
    }

    public List<BoutiqueDecorator> getExtraOrder() {
      return extraOrder;
    }

    public List<Bouquet> getTotalOrder() {
      return totalOrder;
    }

  public Updatable getClient() {
    return client;
  }
}

