package com.epam;

import com.epam.control.CallCenter;
import com.epam.utils.BouquetType;
import com.epam.utils.Card;
import com.epam.utils.Location;
import com.epam.model.user.User;

public class Demo {

  public static void main(String[] args) {
    new CallCenter().phoneCall(new User("John", Card.GOLD, Location.GB));
    /*
    BouquetType b = null;
    for(BouquetType br : BouquetType.values()) {
      if(br.ordinal() == 0)
        b = br;
    }
    System.out.println(b);

     */
  }
}
